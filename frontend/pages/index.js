import React, { useState, useEffect } from 'react';
import Head from 'next/head';
import { Form, Button } from 'react-bootstrap';

import PieChart from '../components/PieChart';
import BarChart from '../components/BarChart';

import styles from '../styles/Home.module.css'

export default function Home() {
  const [csvData, setCsvData] = useState([]);
  const [brands, setBrands] = useState([]);
  const [salesByBrand, setSalesByBrand] = useState([]);

  //create a reference to the file input form element
  const fileRef = React.createRef()

  //base64 is one of the oldest encoding algorithms.
  //toBase64 function here will allow us to encode our uploaded file into a base64String with which we can pass to our backend api.
  const toBase64 = (file) => new Promise((resolve, reject) => {
      //FileReader will allow us to read into our file and translate our file into a base64String.
      const reader = new FileReader()
      //We pass our file into the readAsDataURL() method of our FileReader to actually convert our file into base64 string.
      reader.readAsDataURL(file)
      //onload runs when the translation or conversion from file to base64 is completed.
      //the base64String is now in the result property of our reader.
      reader.onload = () => {

        console.log(reader.result)

       return resolve(reader.result)

      }

      //onerror will run if the conversion of the file to base64 is not completed and an error has been thrown.
      reader.onerror = error => reject(error)
  })

  //function to submit CSV to API for conversion to JSON data
  function uploadCSV(e){
    e.preventDefault()
    //fileRef.current.files[0] - a reference to input element where the file was uploaded with.
    toBase64(fileRef.current.files[0])
    .then(encodedFile => {
      //console.log(encodedFile)
      fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}/api/cars`, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({csvData: encodedFile})
      })
      .then(res => res.json())
      .then(data => {

          //CSV was read and converted to an array of JSON in the backend, this is the reason the backend's response is an array of JSON instead of a csv file.
          console.log(data)

          if(data.jsonArray.length > 0){
            //set raw response of our API as the csvdata state
            setCsvData(data.jsonArray)
          }
      })
    })
  }


  //This useEffect will run on initial render and when our csvData state is updated.
  useEffect(() => {
    //This useEffect will try to get all the distinct brands/makes of the cars in our csvData.
    //makes will be our temporary array where we will push the distinct brands available.
    let makes = []

    //iterate over all the items in the csvData array (which contains data coming from our csv.)
    csvData.forEach(element => {
      //If statement will push the distinct brands into our temporary makes array.
      //If the current element/item being iterated's make property is found within the makes array, the make or brand will not be pushed into the temporary makes array.
      if(!makes.find(make => make === element.make)){
        //push car Make info to the makes array
        makes.push(element.make)
      }
    })
    //console.log(makes)
    setBrands(makes)
  }, [csvData])

  //We're going get the accumulated sales for each brand.
  //Create a useEffect that will run on initial render and when the brands state is updated
  useEffect(()=>{

      //We will set and update our salesByBrand state by using map. This map will return an object which will contain the brand name and its appropriate sales.
      setSalesByBrand(brands.map(brand => {

        //Set/create a variable which will store the accumulated sales of a brand:
        let sales = 0

        //iterate over all the items in the csvData array
        //Putting forEach inside a map will result in a nested loop:
        //brands[0] -> forEach() will be finished first -> brands[1]
        csvData.forEach(element => {

          //For each element in the array we will accumulate the sales per brand.
          //if the current item being iterated by forEach() has the same brand/make with the current item/brand being iterated by the map, we will accumulate the element's sales into the sales variable. However, if the make property or the brand of the current item iterated by forEach does not match the current brand being iterated by map, we will simply do nothing.

          if(element.make === brand){

            sales += parseInt(element.sales)

          }


        })

        //console.log(`${brand}: ${sales}`)

        return {

          brand: brand,
          sales: sales

        }



      }))

  },[brands])



  //check state values after file upload
/* console.log(csvData)
  console.log(brands)
  console.log(salesByBrand)*/

  /*
    Mini-Activity:

    -Pass our salesByBrand state as a prop to our PieChart component
    -The propName is salesData


  */

  return (
            <React.Fragment>
                <Head>
                  <title>CSV Data Visualization</title>
                </Head>

                <Form onSubmit={(e) => uploadCSV(e)}>
                  <Form.Group controlId="csvUploadForm">
                      <Form.Label>Upload CSV: </Form.Label>
                      <Form.Control
                          type="file"
                          ref={ fileRef }
                          accept="csv"
                          required
                      />
                  </Form.Group>
                  <Button variant="primary" type="submit" id="submitBtn">
                      Submit
                  </Button>
                </Form>
                <PieChart salesData={salesByBrand} />
                <BarChart rawData={csvData} />
            </React.Fragment>
  )
}
